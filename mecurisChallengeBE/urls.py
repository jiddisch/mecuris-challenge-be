from django.contrib import admin
from django.conf.urls import url
from graphene_django.views import GraphQLView
from mecurisChallengeBE.schema import schema
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^graphql', csrf_exempt(GraphQLView.as_view(
        graphiql=True, schema=schema))),
]
