import graphene
from graphene_django.types import DjangoObjectType, ObjectType
from .models import Seawheel
from graphql import GraphQLError


class SeawheelType(DjangoObjectType):
    class Meta:
        model = Seawheel


class Query(ObjectType):
    seawheels = graphene.List(SeawheelType)

    def resolve_seawheels(self, info, **kwargs):
        return Seawheel.objects.all()


class SeawheelInput(graphene.InputObjectType):
    name = graphene.String()
    color = graphene.String()


class CreateSeawheel(graphene.Mutation):
    class Arguments:
        input = SeawheelInput(required=True)

    ok = graphene.Boolean()
    seawheel = graphene.Field(SeawheelType)

    @staticmethod
    def mutate(root, info, input=None):
        ok = True
        if not input.name or not input.color:
            raise GraphQLError("name/color field is missing")
        seawheel_instance = Seawheel(name=input.name, color=input.color)
        seawheel_instance.save()

        return CreateSeawheel(ok=ok, seawheel=seawheel_instance)


class DeleteSeawheel(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    ok = graphene.Boolean()

    @classmethod
    def mutate(cls, root, info, **kwargs):
        seawheel = Seawheel.objects.get(pk=kwargs["id"])
        print(seawheel)
        seawheel.delete()
        return cls(ok=True)


class Mutation(graphene.ObjectType):
    create_seawheel = CreateSeawheel.Field()
    delete_seawheel = DeleteSeawheel.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
