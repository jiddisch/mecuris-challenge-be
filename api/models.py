from django.db import models


class Seawheel(models.Model):
    name = models.CharField(max_length=60)
    color = models.CharField(max_length=7)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
